package com.group.project;
import java.util.*;

public class Listings  {
	private int propertyNo;
	private String propertyType;
	private double price;
	private int id;

	public Listings(int propertyNo, String propertyType, double price, int id) {
		this.propertyNo = propertyNo;
		this.propertyType = propertyType;
		this.price = price;
		this.id = id;

	}

	public int getPropertyNo() {
		return propertyNo;
	}

	public void setPropertyNo(int propertyNo) {
		this.propertyNo = propertyNo;
	}

	public String getPropertyType() {
		return propertyType;
	}

	public void setPropertyType(String propertyType) {
		this.propertyType = propertyType;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Listings [propertyNo=" + propertyNo + ", propertyType="
				+ propertyType + ", price=" + price + ", id=" + id + "]";
	}


}

